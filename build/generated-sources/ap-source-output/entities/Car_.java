package entities;

import entities.CarPK;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2018-05-25T15:54:57")
@StaticMetamodel(Car.class)
public class Car_ { 

    public static volatile SingularAttribute<Car, Double> width;
    public static volatile SingularAttribute<Car, Double> length;
    public static volatile SingularAttribute<Car, String> model;
    public static volatile SingularAttribute<Car, String> category;
    public static volatile SingularAttribute<Car, CarPK> carPK;
    public static volatile SingularAttribute<Car, String> manufacturer;
    public static volatile SingularAttribute<Car, Boolean> isfavourite;

}