package entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.6.1.v20150605-rNA", date="2018-05-25T15:54:57")
@StaticMetamodel(Parkingslot.class)
public class Parkingslot_ { 

    public static volatile SingularAttribute<Parkingslot, Boolean> isavailable;
    public static volatile SingularAttribute<Parkingslot, Boolean> isrestricted;
    public static volatile SingularAttribute<Parkingslot, String> address;
    public static volatile SingularAttribute<Parkingslot, Double> latitude;
    public static volatile SingularAttribute<Parkingslot, Double> width;
    public static volatile SingularAttribute<Parkingslot, Double> length;
    public static volatile SingularAttribute<Parkingslot, Integer> id;
    public static volatile SingularAttribute<Parkingslot, Double> longitude;
    public static volatile SingularAttribute<Parkingslot, Boolean> ispayant;

}