/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daniele
 */
@Entity
@Table(name = "ranking")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Ranking.findAll", query = "SELECT r FROM Ranking r")
    , @NamedQuery(name = "Ranking.findByUserid", query = "SELECT r FROM Ranking r WHERE r.userid = :userid")
    , @NamedQuery(name = "Ranking.findByLevel", query = "SELECT r FROM Ranking r WHERE r.level = :level")
    , @NamedQuery(name = "Ranking.findByExp", query = "SELECT r FROM Ranking r WHERE r.exp = :exp")})
public class Ranking implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "userid")
    private Integer userid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "level")
    private int level;
    @Basic(optional = false)
    @NotNull
    @Column(name = "exp")
    private int exp;

    public Ranking() {
    }

    public Ranking(Integer userid) {
        this.userid = userid;
    }

    public Ranking(Integer userid, int level, int exp) {
        this.userid = userid;
        this.level = level;
        this.exp = exp;
    }

    public Integer getUserid() {
        return userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (userid != null ? userid.hashCode() : 0);
        return hash;
    }
    
    public int getLevelBaseOnExp(){
        return decFact(1);
    }


    private int decFact(int newlevel){
        if((newlevel+1)*newlevel*10<exp)
            return this.decFact( newlevel+1);
        else return newlevel;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Ranking)) {
            return false;
        }
        Ranking other = (Ranking) object;
        if ((this.userid == null && other.userid != null) || (this.userid != null && !this.userid.equals(other.userid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Ranking[ userid=" + userid + " ]";
    }
    
}
