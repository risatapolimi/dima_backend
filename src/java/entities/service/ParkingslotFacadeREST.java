/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import entities.Car;
import entities.CarClient;
import entities.CarPK;
import entities.Indexes;
import entities.Parkingslot;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.function.Function;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("entities.parkingslot")
public class ParkingslotFacadeREST extends AbstractFacade<Parkingslot> {

    @EJB
    private IndexesFacade indexFacade;

    @PersistenceContext(unitName = "ParkShareServicePU")
    private EntityManager em;

    public ParkingslotFacadeREST() {
        super(Parkingslot.class);
    }
    
    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response createResponse(String ent) {
        System.out.println(ent);
        Parkingslot entity=gson.fromJson(ent, Parkingslot.class);
        System.out.println(entity.toString());
        switch(entity.getAdvicedDimension()){
            case 0:
                entity.setLength(entity.getLength()*0.99);
                entity.setWidth(entity.getWidth()*0.99);
                break;
            case 1:
                entity.setLength(entity.getLength()*1.25);
                entity.setWidth(entity.getWidth()*1.25);
                break;
            case 2:
                entity.setLength(entity.getLength()*1.5);
                entity.setWidth(entity.getWidth()*1.5);
                break;
        }
        //Take id from indexes table
        int id=indexFacade.find("parking").getValue();
        //set id
        entity.setId(id);
        //update id
        id++;
        indexFacade.edit(new Indexes("parking",id));
        //Create parking 
        this.create(entity);
        String jsoncar=gson.toJson(entity);
        return Response.ok(jsoncar).build();
    }

    
    @Override
    public void create(Parkingslot entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response editResponse(@PathParam("id") Integer id,String park) {
        Parkingslot entity=gson.fromJson(park,Parkingslot.class);
        Parkingslot entitycheck=super.find(id);
        if(entitycheck==null)
            return Response.serverError().entity("Parking not found in the server, wait update from server or contact the assistance").build();
        else{
            System.out.println("Aggiornato parking "+id);
            return Response.ok(gson.toJson(super.edit(entity))).build();
        }
    }

    @DELETE
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response removeResponse(@PathParam("id") Integer id) {
        Parkingslot entity=super.find(id);
        if(entity==null)
            return Response.serverError().entity("Parking not found in the server, wait update from server or contact the assistance").build();
        else{
            super.remove(entity);
            String json=gson.toJson(entity);
        //Settare meglio la remove
            if(super.find(entity.getId())==null)
                return Response.ok(json).build();
            else return Response.serverError().entity("Error: Parking wasn't deleted --> Try again or contact the assistance").build();
        }
        
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findParking(@PathParam("id") Integer id) {
        return Response.ok(gson.toJson(super.find(id))).build();
    }

    @Override
    public List<Parkingslot> findAll() {
        return super.findAll();
    }
    
    @GET
    @Path("all")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findAllParking() {
        return Response.ok(gson.toJson(super.findAll())).build();
    }
    
    @GET
    @Path("list/{lat}/{lon}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response getparkingList(@PathParam("lat") String lat,@PathParam("lon") String lon) {
        
        double latitude=Double.parseDouble(lat);
        double lat1=latitude-0.1;
        double lat2=latitude+0.1;
        double longitude=Double.parseDouble(lon);
        double lon1=longitude-0.1;
        double lon2=longitude+0.1;
        String query="Parkingslot.findByRangeLatLong";
        List<Parkingslot> list=getEntityManager().createNamedQuery(query, entityClass).setParameter("lat1",lat1).setParameter("lat2", lat2)
                                                  .setParameter("lon1",lon1).setParameter("lon2",lon2).getResultList();
        list.forEach(p->p.differenceKm(latitude, longitude));
        Collections.sort(list, (Parkingslot o1, Parkingslot o2) -> {
            if(o1.getDistance()<o2.getDistance())
                return -1;
            else if(o1.getDistance()>o2.getDistance())
                return 1;
            else return 0;
        });     
        
        System.out.println("Invio lista dei parcheggi: "+list.size());
        String jsonlist=gson.toJson(list);
        return Response.ok(jsonlist).build();
    }
    

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Parkingslot> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public Response createResponse(Parkingslot entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
