/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import entities.Car;
import entities.CarClient;
import entities.CarPK;
import entities.Indexes;
import entities.Parkingslot;
import entities.Token;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Response;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("entities.car")
public class CarFacadeREST extends AbstractFacade<Car> {
    
    @EJB
    private IndexesFacade indexFacade;

    @PersistenceContext(unitName = "ParkShareServicePU")
    private EntityManager em;

    private CarPK getPrimaryKey(PathSegment pathSegment) {
        /*
         * pathSemgent represents a URI path segment and any associated matrix parameters.
         * URI path part is supposed to be in form of 'somePath;carid=caridValue;userid=useridValue'.
         * Here 'somePath' is a result of getPath() method invocation and
         * it is ignored in the following code.
         * Matrix parameters are used as field names to build a primary key instance.
         */
        entities.CarPK key = new entities.CarPK();
        javax.ws.rs.core.MultivaluedMap<String, String> map = pathSegment.getMatrixParameters();
        java.util.List<String> carid = map.get("carid");
        if (carid != null && !carid.isEmpty()) {
            key.setCarid(carid.get(0));
        }
        java.util.List<String> userid = map.get("userid");
        if (userid != null && !userid.isEmpty()) {
            key.setUserid(userid.get(0));
        }
        return key;
    }

    public CarFacadeREST() {
        super(Car.class);
    }
    
    @POST
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response createResponse(String ent) {
        
        System.out.println(ent);
        CarClient entity=gson.fromJson(ent, CarClient.class);
        System.out.println(entity.toString());
        //Take id from indexes table
        int id=indexFacade.find("car").getValue();
        //set id
        CarPK carPK=new CarPK(""+id,entity.getUserid());
        Car car=new Car(carPK,entity.getManufacturer(),entity.getModel(),entity.isIsfavourite(),
                            entity.getWidth(),entity.getLength(),entity.getCategory());
        String query="Car.findByUserid";
        List<Car> list=getEntityManager().createNamedQuery(query, entityClass).setParameter("userid", entity.getUserid()).getResultList();
        if(list.isEmpty()){     
            car.setIsfavourite(true);
            entity.setIsfavourite(true); 
        }
        //update id
        id++;
        indexFacade.edit(new Indexes("car",id));
        //Create parking 
        this.create(car);
        entity.setCarid(car.getCarPK().getCarid());
        String jsoncar=gson.toJson(entity);
        System.out.println(jsoncar);
        return Response.ok(jsoncar).build();
    }

    
    @Override
    public void create(Car entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response editResponse(@PathParam("id") PathSegment id, String ent) {
        entities.CarPK key = getPrimaryKey(id);
        CarClient entity=gson.fromJson(ent, CarClient.class);     
        Car entitycheck=super.find(key);
        if(entity.isIsfavourite()){
            String query="Car.updateByUserId";
            getEntityManager().createNamedQuery(query, entityClass).setParameter("userid", entity.getUserid()).setParameter("state", false).setParameter("carid", entity.getCarid()).executeUpdate();
        }
        if(entitycheck==null)
            return Response.serverError().entity("Car not found in the server, delete and reinsert the car or contact the assistance").build();
        else{
            Car car=new Car(key,entity.getManufacturer(),entity.getModel(),entity.isIsfavourite(),
                            entity.getWidth(),entity.getLength(),entity.getCategory());
            Car updatedCar=super.edit(car);
            if(car.equals(updatedCar))
                return Response.ok(ent).build();
            else return Response.serverError().entity("Car update error, delete and reinsert the car or contact the assistance").build();
        }
        
    }

    @DELETE
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response remove(@PathParam("id") PathSegment id) {
        entities.CarPK key = getPrimaryKey(id);
        System.out.println(key.toString());
        Car c=super.find(key);
        if(c==null)
            return Response.serverError().entity("Car not found in the server, wait update from server or contact the assistance").build();
        else{
            super.remove(c);
        //Settare meglio la remove
            if(super.find(c.getCarPK())==null){
                CarClient car=new CarClient(c.getCarPK().getUserid(),c.getCarPK().getCarid(),c.getManufacturer(),c.getModel(),
                                        c.getWidth(),c.getLength(),c.getIsfavourite(),c.getCategory());
                String jsoncar=gson.toJson(car);
                return Response.ok(jsoncar).build();
            }
            else return Response.serverError().entity("Error: Car wasn't deleted --> Try again or contact the assistance").build();
        }
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response find(@PathParam("id") PathSegment id) {
        entities.CarPK key = getPrimaryKey(id);
        Car car= super.find(key);
        if(car!=null){
            String jsoncar = gson.toJson(car);
            System.out.println(jsoncar);
            return Response.ok(jsoncar).build();
        }else{
            String jsonerror = gson.toJson("Error: Car wasn't found --> Try again or contact the assistance");
            return Response.serverError().entity(jsonerror).build();
        }
    }

    @GET
    @Path("all/{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response findAllCar(@PathParam("id") String userid) {
        String query="Car.findByUserid";
        List<Car> list=getEntityManager().createNamedQuery(query, entityClass).setParameter("userid", userid).getResultList();
        List<CarClient> realList=new ArrayList<>();
        for(Car c:list){
            realList.add(new CarClient(c.getCarPK().getUserid(),c.getCarPK().getCarid(),c.getManufacturer(),c.getModel(),
                                        c.getWidth(),c.getLength(),c.getIsfavourite(),c.getCategory()));
        }
        String jsonlist=gson.toJson(realList);
        return Response.ok(jsonlist).build();
    }
    
    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Car> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Car> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public Response createResponse(Car entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    
}
