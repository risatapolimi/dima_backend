/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import com.google.gson.Gson;
import com.sun.corba.se.impl.orbutil.ObjectWriter;
import entities.Indexes;
import entities.Ranking;
import entities.User;
import entities.Token;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("entities.user")
public class UserFacadeREST extends AbstractFacade<User> {
    
    @EJB
    private IndexesFacade indexFacade;
    
    @EJB
    private TokenFacadeREST tokenFacade;
    
    @EJB
    private RankingFacadeREST rankingFacade; 

    @PersistenceContext(unitName = "ParkShareServicePU")
    private EntityManager em;

    public UserFacadeREST() {
        super(User.class);
    }

    
    @POST 
    @Path("registration")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response registerUser(String gsonuser){
        User user=gson.fromJson(gsonuser, User.class);
        return this.createResponse(user);
    }
    
    
    @Override
    public Response createResponse(User entity) {
        try{
            super.findByParameter("User.findByEmail","email",entity.getEmail());
            //Trovata email uguale 
            //rispondo che esiste già tale mail
           
            return Response.status(Response.Status.BAD_REQUEST).entity("Mail già usata, usa un'altra mail o recupera password").build();
        }catch(NoResultException ex){
            //Genera e setta user id 
            int id=indexFacade.find("user").getValue();
            //set id
            entity.setUserid(""+id);
            Ranking ranking = new Ranking(id,1,1);
            //update id
            id++;
            indexFacade.edit(new Indexes("user",id));
            //Crea entity nel database
            //response di corretto utente creato e invio il TOKEN
            this.create(entity);
            //Creazione Ranking nuovo utente
            
            this.rankingFacade.create(ranking);
            
            
            String gsonuser=gson.toJson(entity);
            return Response.ok(gsonuser).build();
        }   
    }
    
    
    @Override
    public void create(User entity) {
         super.create(entity); 
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response edit(@PathParam("id") String id, User entity) {
        User entitycheck=super.find(id);
        if(entitycheck==null)
            return Response.serverError().entity("User not found in the server, contact the assistance").build();
        else{
            return Response.ok(super.edit(entity)).build();
        }
    }

    @DELETE
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response remove(@PathParam("id") String id) {
        User entity=super.find(id);
        if(entity==null)
            return Response.serverError().entity("User not found in the server,contact the assistance").build();
        else{
            super.remove(entity);
        //Settare meglio la remove
            if(super.find(entity.getUserid())==null)
                return Response.ok(entity).build();
            else return Response.serverError().entity("Error: User wasn't deleted --> Try again or contact the assistance").build();
        }
    }
    
    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response find(@PathParam("id") String id) {
        User user=super.find(id);
        if(user!=null){
            String jsonuser = gson.toJson(user);
            System.out.println(jsonuser);
            return Response.ok(jsonuser).build();
        }else{
            String jsonerror = gson.toJson("Error: User wasn't found --> Try again or contact the assistance");
            return Response.serverError().entity(jsonerror).build();
        }
    }
    
    @GET
    @Path("login/{email}/{passwd}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response login(@PathParam("email") String email,@PathParam("passwd") String passwd) {
        System.out.println("Richiesta arriva");
        try{
            User entityfound=super.findByParameter("User.findByEmail","email",email);
            if(entityfound.getPwdHash().equals(passwd)){
                Token token=new Token(entityfound.getUserid(),false);
                if(this.tokenFacade.find(token.getUserid())!=null)
                    this.tokenFacade.edit(token);
                else tokenFacade.create(token);
                // 2. Java object to JSON, and assign to a String
                String jsonToken = gson.toJson(token);
               return Response.ok(jsonToken).build();
            }    
            else {
                // 2. Java object to JSON, and assign to a String
            return Response.status(Response.Status.BAD_REQUEST).entity("Password errata,riprova").build();
            }
        }catch(NoResultException ex){
                // 2. Java object to JSON, and assign to a String
            return Response.status(Response.Status.BAD_REQUEST).entity("Mail errata,riprova").build();
        }
        
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<User> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<User> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    
    
}
