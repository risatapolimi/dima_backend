/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities.service;

import entities.Ranking;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Daniele
 */
@Stateless
@Path("entities.ranking")
public class RankingFacadeREST extends AbstractFacade<Ranking> {

    @PersistenceContext(unitName = "ParkShareServicePU")
    private EntityManager em;

    public RankingFacadeREST() {
        super(Ranking.class);
    }

    @POST
    @Override
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public void create(Ranking entity) {
        super.create(entity);
    }

    @PUT
    @Path("{id}")
    @Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response edit(@PathParam("id") Integer id) {
        Ranking ranking=super.find(id);
        if(ranking!=null){
            ranking.setExp(ranking.getExp()+7);
            ranking.setLevel(ranking.getLevelBaseOnExp());
            super.edit(ranking);
            return Response.ok(gson.toJson(ranking)).build();
        }else{
            String jsonerror = gson.toJson("Error: Ranking wasn't found --> Try again or contact the assistance");
            return Response.serverError().entity(jsonerror).build();
        }
    }

    @DELETE
    @Path("{id}")
    public void remove(@PathParam("id") Integer id) {
        super.remove(super.find(id));
    }

    @GET
    @Path("{id}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Response find(@PathParam("id") Integer id) {
        Ranking rank=super.find(id);
        if(rank!=null){
            String jsonrank = gson.toJson(rank);
            return Response.ok(jsonrank).build();
        }else{
            String jsonerror = gson.toJson("Error: Ranking wasn't found --> Try again or contact the assistance");
            return Response.serverError().entity(jsonerror).build();
        }
    }

    @GET
    @Override
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Ranking> findAll() {
        return super.findAll();
    }

    @GET
    @Path("{from}/{to}")
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public List<Ranking> findRange(@PathParam("from") Integer from, @PathParam("to") Integer to) {
        return super.findRange(new int[]{from, to});
    }

    @GET
    @Path("count")
    @Produces(MediaType.TEXT_PLAIN)
    public String countREST() {
        return String.valueOf(super.count());
    }

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    @Override
    public Response createResponse(Ranking entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
