/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import java.util.Comparator;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daniele
 */
@Entity
@Table(name = "parkingslot")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Parkingslot.findAll", query = "SELECT p FROM Parkingslot p")
    , @NamedQuery(name = "Parkingslot.findByLatitude", query = "SELECT p FROM Parkingslot p WHERE p.latitude = :latitude")
    , @NamedQuery(name = "Parkingslot.findByLongitude", query = "SELECT p FROM Parkingslot p WHERE p.longitude = :longitude")
    , @NamedQuery(name = "Parkingslot.findByWidth", query = "SELECT p FROM Parkingslot p WHERE p.width = :width")
    , @NamedQuery(name = "Parkingslot.findByLength", query = "SELECT p FROM Parkingslot p WHERE p.length = :length")
    , @NamedQuery(name = "Parkingslot.findByIsavailable", query = "SELECT p FROM Parkingslot p WHERE p.isavailable = :isavailable")
    , @NamedQuery(name = "Parkingslot.findByIspayant", query = "SELECT p FROM Parkingslot p WHERE p.ispayant = :ispayant")
    , @NamedQuery(name = "Parkingslot.findByIsrestricted", query = "SELECT p FROM Parkingslot p WHERE p.isrestricted = :isrestricted")
    , @NamedQuery(name = "Parkingslot.findById", query = "SELECT p FROM Parkingslot p WHERE p.id = :id")
    , @NamedQuery(name = "Parkingslot.findByRangeLatLong", query = "SELECT p FROM Parkingslot p WHERE (p.latitude BETWEEN :lat1 AND :lat2) AND (p.longitude BETWEEN :lon1 AND :lon2) ")
    , @NamedQuery(name = "Parkingslot.findByAddress", query = "SELECT p FROM Parkingslot p WHERE p.address = :address")})
public class Parkingslot implements Serializable {

    private static final long serialVersionUID = 1L;
    @Basic(optional = false)
    @NotNull
    @Column(name = "latitude")
    private double latitude;
    @Basic(optional = false)
    @NotNull
    @Column(name = "longitude")
    private double longitude;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "width")
    private Double width;
    @Column(name = "length")
    private Double length;
    @Column(name = "isavailable")
    private Boolean isavailable;
    @Column(name = "ispayant")
    private Boolean ispayant;
    @Column(name = "isrestricted")
    private Boolean isrestricted;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id")
    private Integer id;
    @Size(max = 50)
    @Column(name = "address")
    private String address;
    
    @Transient
    private int advicedDimension;
    @Transient
    private double distance;

    public Parkingslot() {
    }

    public Parkingslot(Integer id) {
        this.id = id;
    }

    public Parkingslot(Integer id, double latitude, double longitude) {
        this.id = id;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public Boolean getIsavailable() {
        return isavailable;
    }

    public void setIsavailable(Boolean isavailable) {
        this.isavailable = isavailable;
    }

    public Boolean getIspayant() {
        return ispayant;
    }

    public void setIspayant(Boolean ispayant) {
        this.ispayant = ispayant;
    }

    public Boolean getIsrestricted() {
        return isrestricted;
    }

    public void setIsrestricted(Boolean isrestricted) {
        this.isrestricted = isrestricted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getAdvicedDimension() {
        return advicedDimension;
    }

    public void setAdvicedDimension(int advicedDimension) {
        this.advicedDimension = advicedDimension;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Parkingslot)) {
            return false;
        }
        Parkingslot other = (Parkingslot) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Parkingslot{" + "latitude=" + latitude + ", longitude=" + longitude + ", width=" + width + ", length=" + length + ", id=" + id + ", address=" + address + ", advicedDimension=" + advicedDimension + '}';
    }
    
    
    public void differenceKm(double lat,double lon){
        double pigreco = 3.1415927;
        double lat1= pigreco * this.getLatitude() /180;
        double long1=pigreco * this.getLongitude() /180;
        double lat2= pigreco * lat /180;
        double long2=pigreco * lon /180;
        double deltalat=lat1-lat2;
        double deltalong=long1-long2;
        double distRadianti=Math.asin(
                (
                        Math.sqrt(
                                Math.pow(Math.cos(lat2)*Math.sin(deltalong),2)
                                + Math.pow(Math.cos(lat1)*Math.sin(lat2)-Math.sin(lat1)*Math.cos(lat2)*Math.cos(deltalong),2)
                        )
                )
                        /
                        (Math.sin(lat1)*Math.sin(lat2)+Math.cos(lat1)*Math.cos(lat2)*Math.cos(deltalong))
        );

          
        distance=distRadianti*6371;
        
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }
   

}
