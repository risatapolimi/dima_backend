/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Daniele
 */

@Entity
@Table(name = "car")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Car.findAll", query = "SELECT c FROM Car c")
    , @NamedQuery(name = "Car.findByManufacturer", query = "SELECT c FROM Car c WHERE c.manufacturer = :manufacturer")
    , @NamedQuery(name = "Car.findByModel", query = "SELECT c FROM Car c WHERE c.model = :model")
    , @NamedQuery(name = "Car.findByWidth", query = "SELECT c FROM Car c WHERE c.width = :width")
    , @NamedQuery(name = "Car.findByLength", query = "SELECT c FROM Car c WHERE c.length = :length")
    , @NamedQuery(name = "Car.findByIsfavourite", query = "SELECT c FROM Car c WHERE c.isfavourite = :isfavourite")
    , @NamedQuery(name = "Car.findByCategory", query = "SELECT c FROM Car c WHERE c.category = :category")
    , @NamedQuery(name = "Car.findByCarid", query = "SELECT c FROM Car c WHERE c.carPK.carid = :carid")
    , @NamedQuery(name = "Car.updateByUserId", query = "UPDATE Car c SET c.isfavourite= :state WHERE c.carPK.userid=:userid and c.carPK.carid <> :carid")
    , @NamedQuery(name = "Car.findByUserid", query = "SELECT c FROM Car c WHERE c.carPK.userid = :userid")})
public class Car implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected CarPK carPK;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "manufacturer")
    private String manufacturer;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "model")
    private String model;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "width")
    private Double width;
    @Column(name = "length")
    private Double length;
    @Basic(optional = false)
    @NotNull
    @Column(name = "isfavourite")
    private boolean isfavourite;
    @Size(max = 50)
    @Column(name = "category")
    private String category;

    public Car() {
    }

    public Car(CarPK carPK) {
        this.carPK = carPK;
    }

    public Car(CarPK carPK, String manufacturer, String model, boolean isfavourite) {
        this.carPK = carPK;
        this.manufacturer = manufacturer;
        this.model = model;
        this.isfavourite = isfavourite;
    }
    
    public Car(CarPK carPK, String manufacturer, String model, boolean isfavourite,Double w,Double l,String cat) {
        this.carPK = carPK;
        this.manufacturer = manufacturer;
        this.model = model;
        this.isfavourite = isfavourite;
        this.width=w;
        this.length=l;
        this.category=cat;
    }

    public Car(String carid, String userid) {
        this.carPK = new CarPK(carid, userid);
    }

    public CarPK getCarPK() {
        return carPK;
    }

    public void setCarPK(CarPK carPK) {
        this.carPK = carPK;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Double getWidth() {
        return width;
    }

    public void setWidth(Double width) {
        this.width = width;
    }

    public Double getLength() {
        return length;
    }

    public void setLength(Double length) {
        this.length = length;
    }

    public boolean getIsfavourite() {
        return isfavourite;
    }

    public void setIsfavourite(boolean isfavourite) {
        this.isfavourite = isfavourite;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (carPK != null ? carPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Car)) {
            return false;
        }
        Car other = (Car) object;
        if ((this.carPK == null && other.carPK != null) || (this.carPK != null && !this.carPK.equals(other.carPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "entities.Car[ carPK=" + carPK + " ]";
    }
    
}
