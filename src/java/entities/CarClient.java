/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

/**
 *
 * @author Daniele
 */
public class CarClient {
    
    protected String userid;
    protected String carid;
    private String manufacturer;
    private String model;
    private Double width;
    private Double length;
    private boolean isfavourite;
    private String category;
    
    public CarClient(String u,String c,String ma,String mo,Double w,Double l,boolean isf,String cat){
        this.userid=u;
        this.carid=c;
        this.manufacturer=ma;
        this.model=mo;
        this.width=w;
        this.length=l;
        this.isfavourite=isf;
        this.category=cat;
    }

    public String getUserid() {
        return userid;
    }

    public String getCarid() {
        return carid;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public String getModel() {
        return model;
    }

    public Double getWidth() {
        return width;
    }

    public Double getLength() {
        return length;
    }

    public boolean isIsfavourite() {
        return isfavourite;
    }

    public String getCategory() {
        return category;
    }

    public void setCarid(String carid) {
        this.carid = carid;
    }

    @Override
    public String toString() {
        return "CarClient{" + "userid=" + userid + ", carid=" + carid + ", manufacturer=" + manufacturer + ", model=" + model + ", width=" + width + ", length=" + length + ", isfavourite=" + isfavourite + ", category=" + category + '}';
    }

    public void setIsfavourite(boolean b) {
        this.isfavourite=b;
    }
    
    
}
